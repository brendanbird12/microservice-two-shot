import React from "react";


function HatsList(props) {
    constructor(props)
    return(
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Location</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <thead>
                {props.hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{hat.name}</td>
                            <td>{hat.fabric}</td>
                            <td>{hat.color}</td>
                            <td>{hat.picture_url}</td>
                            <td>{hat.location}</td>
                            <button onClick={() => this.delete(hat.id)}></button>
                        </tr>
                    );
                })}
            </thead>
        </table>
    )
}

export default HatsList;