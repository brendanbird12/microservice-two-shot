import React from "react";

function ShoesList(props) {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes?.map(shoe => {
            return (
              <tr key={shoe.href}>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.picture_url }</td>
                <td>{ shoe.bin } </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
  
  export default ShoesList;