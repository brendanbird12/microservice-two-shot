from django.db import models
from requests import options
from django.urls import reverse

# Create your models here.


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=150, null=True)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)


class Hat(models.Model):
    name = models.CharField(max_length=200)
    fabric = models.CharField(default="", max_length=150)
    color = models.CharField(default="", max_length=50)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO, related_name="hats", on_delete=models.CASCADE, null=True)

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})

    def __str__(self):
        return f'This is a {self.color} {self.name} in {self.location}'
