from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Hat, LocationVO
from django.http import JsonResponse
import json

# Create your views here.


class LocationVOModelEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href", 
        "closet_name",
        "section_number", 
        "shelf_number", 
        "id"
    ]


class HatDetailModelEncoder(ModelEncoder):
    model = Hat
    properties = ["name", "fabric", "color", "picture_url", "location", "id"]
    encoders = {"location": LocationVOModelEncoder()}


class HatListModelEncoder(ModelEncoder):
    model = Hat
    properties = ["name", "fabric", "color", "picture_url", "location", "id"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailModelEncoder,
                safe=False 
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Invalid hat ID"}, status=400)
    elif request.method == "DELETE":
        try:
            count, _ = Hat.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Invalid hat ID"}, status=400)
    else: 
        try:
            content = json.loads(request.body)
            Hat.objects.filter(id=pk).update(**content)
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailModelEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Invalid hat ID"}, status=400)


@require_http_methods(["GET", "POST"])
def api_list_hat(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListModelEncoder
        )
    else:
        content = json.loads(request.body)
        print(content)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invaild Location Id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailModelEncoder,
            safe=False
        )
