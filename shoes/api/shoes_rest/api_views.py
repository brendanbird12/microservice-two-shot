from django.http import JsonResponse
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
from common.json import ModelEncoder
import json



class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number", 
        "bin_size",
        "id",
    ]


class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id", 
        "model_name",
        "color", 
        "manufacturer",
        "picture_url" 
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


class ShoesDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "id",
    ]
    encoders = {"bin": BinVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoesListEncoder)
    else: 
        content = json.loads(request.body) 
        try:                                
            bin_href = content["bin"] 
            bin = BinVO.objects.get(import_href=bin_href) 
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoesDetailEncoder,
                safe=False 
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Invalid shoe ID"}, status=400)
    elif request.method == "DELETE":
        try:
            count, _ = Shoe.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Invalid shoe ID"}, status=400)
    else: 
        try:
            content = json.loads(request.body)
            Shoe.objects.filter(id=pk).update(**content)
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoesDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Invalid shoe ID"}, status=400)

