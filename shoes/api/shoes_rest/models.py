from django.db import models
from django.urls import reverse

# Create your models here.

class BinVO(models.Model):
    import_href = models.CharField(max_length=250, unique=True)
    closet_name = models.CharField(max_length=100,null=True)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=25)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name='shoes',
        on_delete=models.CASCADE,
        null=True
    )

    #  need to get the url of the shoe for later
    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})

    def __str__(self):
        return f'This is a {self.color} {self.manufacturer} in {self.bin}'
